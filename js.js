/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//
//$(document).ready(function () {
//    $("p").click(function () {
//        $(this).hide();
//    });
//});



var operators = ["+", "-", "*"];
var number1, number2, result = 0;
var yourChoose = false, isStart = false, isSame = false, isFirst = true;
var score, level;
var timeLimit;
var isProcess;
var widthCountDown = 0;
var wPerSecond = 0;
var delayNe = 100;
var distanceRandom;

var myInterval;
function startGame() {
    if(isFirst){
        widthCountDown = $(".count-down").width();
        isFirst = false;
    }
    $(".count-down").width(widthCountDown);
    timeLimit = 15000;
    distanceRandom = 10;
    score = 0;
    level = 1;
//    isProcess = false when you click btn agree or deny
    isProcess = true;
    document.getElementsByTagName('p')[0].innerHTML = "Correct or Lose ??";
    document.getElementById("score").innerHTML = score;
    document.getElementById("level").innerHTML = level;
    
    wPerSecond = widthCountDown / (timeLimit / delayNe);
    myInterval = setInterval(myGame, delayNe);
    randomGame();
    $("#btnStart").hide();
}

function randomGame() {
    number1 = Math.floor(Math.random() * 2 * distanceRandom) - distanceRandom;
    number2 = Math.floor(Math.random() * 2 * distanceRandom) - distanceRandom;
    let numberOperator = Math.floor(Math.random() * 3);
    let temp = number1 + operators[numberOperator] + number2;
    try {
        result = eval(temp);
    } catch (e) {
        randomGame();
        return;
    }
    let resultRandom = Math.floor(Math.random() * result * 2);
    isSame = result === resultRandom ? true : false;
    document.getElementById("calculator").innerHTML = temp;
    document.getElementById("result").innerHTML = result;
    document.getElementById("randomResult").innerHTML = resultRandom;
    level += 1;
    score += 5;
}
;
//startGame();




function myGame() {
//    if(isStart){
//        randomGame();
//        isStart = false;
//    }    

    let ok = $(".count-down").width();
    ok -= wPerSecond;
    $(".count-down").width(ok);
    if (!isProcess && (yourChoose === isSame)) {
        document.getElementsByTagName('p')[0].innerHTML = "CORRECT~";
        $(".count-down").width(widthCountDown);
        document.getElementById("score").innerHTML = score;
        document.getElementById("level").innerHTML = level;
        if(level%5===0 && level <=15){
            timeLimit -= 3000;
            wPerSecond = widthCountDown / (timeLimit / delayNe);
            distanceRandom+=10;
        }
        randomGame();
        isProcess = true;
    } else if (!isProcess && !(yourChoose === isSame) || ok <= 0) {
        document.getElementsByTagName('p')[0].innerHTML = "LOSE!";
        clearInterval(myInterval);
        $("#btnStart").show();
    }
}



function btnAgree() {
    isProcess = false;
    yourChoose = true;
};

function btnDeny() {
    isProcess = false;
    yourChoose = false;
};